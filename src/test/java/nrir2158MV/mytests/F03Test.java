package nrir2158MV.mytests;

import nrir2158MV.controller.NoteController;
import nrir2158MV.model.Corigent;
import nrir2158MV.model.Elev;
import nrir2158MV.repository.ClasaRepositoryMock;
import nrir2158MV.utils.ClasaException;
import nrir2158MV.utils.Constants;
import org.junit.*;
import org.junit.rules.ExpectedException;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

public class F03Test {
    private ClasaRepositoryMock clasaRepo;
    private NoteController controller;

    private Elev e1 = new Elev(123, "Egon");
    private Elev e2 = new Elev(456, "Ionut");
    private Elev e3 = new Elev(789, "Zapada");

    private HashMap<Elev, HashMap<String, List<Double>>> clasa;

    @Before
    public void setUp() {
        clasa = new HashMap<>();
        clasaRepo = new ClasaRepositoryMock();
        controller = new NoteController();
    }

    @After
    public void tearDown() {
    }

    @Rule
    public ExpectedException expectedEx = ExpectedException.none();

    @Test
    public void testNonValid() throws ClasaException {
        clasaRepo.setClasa(clasa);
        controller.setClasa(clasaRepo);

        expectedEx.expect(ClasaException.class);
        expectedEx.expectMessage(Constants.emptyRepository);
        controller.getCorigenti();
    }

    @Test
    public void testValid() {
        HashMap<String, List<Double>> noteE1 = new HashMap<>();
        noteE1.put("lftc", Arrays.asList(8.0, 7.0));
        noteE1.put("asc", Arrays.asList(10.0, 8.0));
        HashMap<String, List<Double>> noteE2 = new HashMap<>();
        noteE2.put("lftc", Arrays.asList(4.0, 2.5));
        noteE2.put("asc", Arrays.asList(8.0));
        HashMap<String, List<Double>> noteE3 = new HashMap<>();
        noteE3.put("lftc", Arrays.asList(2.5, 5.0));
        noteE3.put("asc", Arrays.asList(4.0));
        clasa.put(e1, noteE1);
        clasa.put(e2, noteE2);
        clasa.put(e3, noteE3);

        clasaRepo.setClasa(clasa);
        controller.setClasa(clasaRepo);

        try {
            List<Corigent> corigenti = controller.getCorigenti();
            Assert.assertEquals(corigenti.size(), 2);
            corigenti.forEach(corigent -> {
                String numeElev = corigent.getNumeElev();
                Assert.assertTrue(numeElev.equals(e2.getNume()) || numeElev.equals(e3.getNume()));
                if (numeElev.equals(e2.getNume())) {
                    Assert.assertEquals(corigent.getNrMaterii(), 1);
                } else {
                    Assert.assertEquals(corigent.getNrMaterii(), 2);
                }
            });
        } catch (ClasaException e) {
            Assert.fail();
        }
    }
}
