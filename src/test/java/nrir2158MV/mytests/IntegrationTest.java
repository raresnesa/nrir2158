package nrir2158MV.mytests;

import nrir2158MV.controller.NoteController;
import nrir2158MV.model.Corigent;
import nrir2158MV.model.Elev;
import nrir2158MV.model.Medie;
import nrir2158MV.model.Nota;
import nrir2158MV.repository.ClasaRepositoryMock;
import nrir2158MV.repository.EleviRepositoryMock;
import nrir2158MV.repository.NoteRepositoryMock;
import nrir2158MV.utils.ClasaException;
import nrir2158MV.utils.Constants;
import org.junit.*;
import org.junit.rules.ExpectedException;

import java.util.*;

import static org.junit.Assert.assertEquals;

public class IntegrationTest {
    private ClasaRepositoryMock clasaRepo;
    private EleviRepositoryMock eleviRepo;
    private NoteRepositoryMock noteRepo;
    private NoteController controller;

    private Elev e1 = new Elev(123, "Egon");
    private Elev e2 = new Elev(456, "Ionut");
    private Elev e3 = new Elev(789, "Zapada");

    private HashMap<Elev, HashMap<String, List<Double>>> clasa;

    @Before
    public void setUp() {
        clasa = new HashMap<>();
        clasaRepo = new ClasaRepositoryMock();
        eleviRepo = new EleviRepositoryMock();
        noteRepo = new NoteRepositoryMock();
        controller = new NoteController();
    }

    @After
    public void tearDown() {
    }

    @Rule
    public ExpectedException expectedEx = ExpectedException.none();

    @Test
    public void testBBA() throws ClasaException {
        expectedEx.expect(ClasaException.class);
        expectedEx.expectMessage(Constants.invalidNota);
        noteRepo.addNota(new Nota(842, "Calcul numeric", 13.4));
    }

    @Test
    public void testBBB() throws ClasaException {
        HashMap<String, List<Double>> noteE1 = new HashMap<>();
        noteE1.put("lftc", Arrays.asList(8.0));
        HashMap<String, List<Double>> noteE2 = new HashMap<>();
        noteE2.put("lftc", Arrays.asList(9.0));
        clasa.put(e1, noteE1);
        clasa.put(e2, noteE2);

        clasaRepo.setClasa(clasa);

        List<Medie> medii = clasaRepo.calculeazaMedii();

        Assert.assertEquals(medii.size(), 2);
        medii.sort(Comparator.comparingInt(m -> m.getElev().getNrmatricol()));
        Medie m1 = medii.get(0);
        Medie m2 = medii.get(1);
        Assert.assertEquals(m1.getElev().getNrmatricol(), e1.getNrmatricol());
        Assert.assertEquals(m1.getElev().getNume(), e1.getNume());
        Assert.assertEquals(m1.getMedie(), 8, 0.001);
        Assert.assertEquals(m2.getElev().getNrmatricol(), e2.getNrmatricol());
        Assert.assertEquals(m2.getElev().getNume(), e2.getNume());
        Assert.assertEquals(m2.getMedie(), 9, 0.001);
    }

    @Test
    public void testBBC() {
        HashMap<String, List<Double>> noteE1 = new HashMap<>();
        noteE1.put("lftc", Arrays.asList(8.0, 7.0));
        noteE1.put("asc", Arrays.asList(10.0, 8.0));
        HashMap<String, List<Double>> noteE2 = new HashMap<>();
        noteE2.put("lftc", Arrays.asList(4.0, 2.5));
        noteE2.put("asc", Arrays.asList(8.0));
        HashMap<String, List<Double>> noteE3 = new HashMap<>();
        noteE3.put("lftc", Arrays.asList(2.5, 5.0));
        noteE3.put("asc", Arrays.asList(4.0));
        clasa.put(e1, noteE1);
        clasa.put(e2, noteE2);
        clasa.put(e3, noteE3);

        clasaRepo.setClasa(clasa);

        try {
            List<Corigent> corigenti = clasaRepo.getCorigenti();
            Assert.assertEquals(corigenti.size(), 2);
            corigenti.forEach(corigent -> {
                String numeElev = corigent.getNumeElev();
                Assert.assertTrue(numeElev.equals(e2.getNume()) || numeElev.equals(e3.getNume()));
                if (numeElev.equals(e2.getNume())) {
                    Assert.assertEquals(corigent.getNrMaterii(), 1);
                } else {
                    Assert.assertEquals(corigent.getNrMaterii(), 2);
                }
            });
        } catch (ClasaException e) {
            Assert.fail();
        }
    }

    @Test
    public void testBBPABC() throws ClasaException {
        // P -> A -> B -> C (A, B, C - valide)
        clasaRepo = (ClasaRepositoryMock) controller.getClasaRepo();
        eleviRepo = (EleviRepositoryMock) controller.getEleviRepo();
        noteRepo = (NoteRepositoryMock) controller.getNoteRepo();

        eleviRepo.addElev(e1);

        // P
        controller.addNota(e1.getNrmatricol(), "ASC", 6);
        List<Nota> note = noteRepo.getNote();
        Assert.assertEquals(note.size(), 1);
        Assert.assertEquals(note.get(0).getNota(), 6.0, 0.001);
        Assert.assertEquals(note.get(0).getNrmatricol(), e1.getNrmatricol(), 0.001);
        Assert.assertEquals(note.get(0).getMaterie(), "ASC");

        // A
        Nota n1 = new Nota(e1.getNrmatricol(), "ASC", 4);
        Nota n2 = new Nota(e1.getNrmatricol(), "ASC", 2.5);
        noteRepo.addNota(n1);
        noteRepo.addNota(n2);
        note = noteRepo.getNote();
        Assert.assertEquals(note.size(), 3);
        Assert.assertNotEquals(note.indexOf(n1), -1);
        Assert.assertNotEquals(note.indexOf(n2), -1);

        // B
        clasaRepo.creazaClasa(eleviRepo.getElevi(), noteRepo.getNote());
        List<Medie> medii = clasaRepo.calculeazaMedii();
        Assert.assertEquals(medii.size(), 1);
        Assert.assertEquals(medii.get(0).getElev(), e1);
        Assert.assertEquals(medii.get(0).getMedie(), (6 + 4 + 2.5) / 3, 0.001);

        // C
        List<Corigent> corigenti = clasaRepo.getCorigenti();
        Assert.assertEquals(corigenti.size(), 1);
        Assert.assertEquals(corigenti.get(0).getNumeElev(), e1.getNume());
        Assert.assertEquals(corigenti.get(0).getNrMaterii(), 1);
    }

    @Test
    public void testTDA() throws ClasaException {
        noteRepo.addNota(new Nota(843, "OOP", 10.0));
        assertEquals(1, noteRepo.getNote().size());
    }

    @Test
    public void testTDB() throws ClasaException {
        clasaRepo.setClasa(clasa);
        expectedEx.expect(ClasaException.class);
        expectedEx.expectMessage(Constants.emptyRepository);
        clasaRepo.calculeazaMedii();
    }

    @Test
    public void testTDC() throws ClasaException {
        clasaRepo.setClasa(clasa);

        expectedEx.expect(ClasaException.class);
        expectedEx.expectMessage(Constants.emptyRepository);
        clasaRepo.getCorigenti();
    }

    @Test
    public void testTDPA() throws ClasaException {
        // P -> A
        eleviRepo = (EleviRepositoryMock) controller.getEleviRepo();
        noteRepo = (NoteRepositoryMock) controller.getNoteRepo();

        eleviRepo.addElev(e1);

        // P
        controller.addNota(e1.getNrmatricol(), "ASC", 6);
        List<Nota> note = noteRepo.getNote();
        Assert.assertEquals(note.size(), 1);
        Assert.assertEquals(note.get(0).getNota(), 6.0, 0.001);
        Assert.assertEquals(note.get(0).getNrmatricol(), e1.getNrmatricol(), 0.001);
        Assert.assertEquals(note.get(0).getMaterie(), "ASC");

        // A
        noteRepo.addNota(new Nota(e1.getNrmatricol(), "VVSS", 1.0));
        assertEquals(2, noteRepo.getNote().size());
    }

    @Test
    public void testTDPAB() throws ClasaException {
        // P -> A -> B
        eleviRepo = (EleviRepositoryMock) controller.getEleviRepo();
        noteRepo = (NoteRepositoryMock) controller.getNoteRepo();

        eleviRepo.addElev(e1);

        // P
        controller.addNota(e1.getNrmatricol(), "ASC", 6);
        List<Nota> note = noteRepo.getNote();
        Assert.assertEquals(note.size(), 1);
        Assert.assertEquals(note.get(0).getNota(), 6.0, 0.001);
        Assert.assertEquals(note.get(0).getNrmatricol(), e1.getNrmatricol(), 0.001);
        Assert.assertEquals(note.get(0).getMaterie(), "ASC");

        // A
        noteRepo.addNota(new Nota(e1.getNrmatricol(), "VVSS", 1.0));
        assertEquals(2, noteRepo.getNote().size());

        // B
        clasaRepo.creazaClasa(eleviRepo.getElevi(), noteRepo.getNote());
        List<Medie> medii = clasaRepo.calculeazaMedii();

        Assert.assertEquals(medii.size(), 1);
        Medie m1 = medii.get(0);
        Assert.assertEquals(m1.getElev().getNrmatricol(), e1.getNrmatricol());
        Assert.assertEquals(m1.getElev().getNume(), e1.getNume());
        Assert.assertEquals(m1.getMedie(), (6.0 + 1.0) / 2.0, 0.001);
    }

    @Test
    public void testTDPABC() throws ClasaException {
        // P -> A -> B
        eleviRepo = (EleviRepositoryMock) controller.getEleviRepo();
        noteRepo = (NoteRepositoryMock) controller.getNoteRepo();

        eleviRepo.addElev(e1);

        // P
        controller.addNota(e1.getNrmatricol(), "ASC", 6);
        List<Nota> note = noteRepo.getNote();
        Assert.assertEquals(note.size(), 1);
        Assert.assertEquals(note.get(0).getNota(), 6.0, 0.001);
        Assert.assertEquals(note.get(0).getNrmatricol(), e1.getNrmatricol(), 0.001);
        Assert.assertEquals(note.get(0).getMaterie(), "ASC");

        // A
        noteRepo.addNota(new Nota(e1.getNrmatricol(), "VVSS", 1.0));
        assertEquals(2, noteRepo.getNote().size());

        // B
        clasaRepo.creazaClasa(eleviRepo.getElevi(), noteRepo.getNote());
        List<Medie> medii = clasaRepo.calculeazaMedii();

        Assert.assertEquals(medii.size(), 1);
        Medie m1 = medii.get(0);
        Assert.assertEquals(m1.getElev().getNrmatricol(), e1.getNrmatricol());
        Assert.assertEquals(m1.getElev().getNume(), e1.getNume());
        Assert.assertEquals(m1.getMedie(), (6.0 + 1.0) / 2.0, 0.001);

        // C
        List<Corigent> corigenti = clasaRepo.getCorigenti();
        Assert.assertEquals(corigenti.size(), 1);
        Assert.assertEquals(corigenti.get(0).getNumeElev(), e1.getNume());
        Assert.assertEquals(corigenti.get(0).getNrMaterii(), 1);
    }
}
