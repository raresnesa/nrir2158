package nrir2158MV.mytests;

import nrir2158MV.model.Elev;
import nrir2158MV.model.Medie;
import nrir2158MV.repository.ClasaRepositoryMock;
import nrir2158MV.utils.ClasaException;
import nrir2158MV.utils.Constants;
import org.junit.*;
import org.junit.rules.ExpectedException;
import org.junit.runners.JUnit4;

import java.util.*;

public class F02Test {
    private ClasaRepositoryMock repo;

    private Elev e1 = new Elev(123, "Ben");
    private Elev e2 = new Elev(456, "Dover");

    private HashMap<Elev, HashMap<String, List<Double>>> clasa;
    private List<Medie> medii;
    private Medie m1;
    private Medie m2;

    @Before
    public void setUp() throws Exception {
        repo = new ClasaRepositoryMock();
        clasa = new HashMap<>();
    }

    @After
    public void tearDown() throws Exception {
    }

    @Rule
    public ExpectedException expectedEx = ExpectedException.none();

    @Test
    public void test1() throws ClasaException {
        repo.setClasa(clasa);
        expectedEx.expect(ClasaException.class);
        expectedEx.expectMessage(Constants.emptyRepository);
        repo.calculeazaMedii();
    }

    @Test
    public void test2() throws ClasaException {
        HashMap<String, List<Double>> noteE1 = new HashMap<>();
        noteE1.put("lftc", new ArrayList<Double>());
        noteE1.put("asc", new ArrayList<Double>());
        clasa.put(e1, noteE1);

        repo.setClasa(clasa);
        medii = repo.calculeazaMedii();

        Assert.assertEquals(medii.size(), 1);
        m1 = medii.get(0);
        Assert.assertEquals(m1.getElev().getNrmatricol(), e1.getNrmatricol());
        Assert.assertEquals(m1.getElev().getNume(), e1.getNume());
        Assert.assertEquals(m1.getMedie(), 0, 0.001);
    }

    @Test
    public void test3() throws ClasaException {
        HashMap<String, List<Double>> noteE1 = new HashMap<>();
        noteE1.put("lftc", Arrays.asList(8.0));
        HashMap<String, List<Double>> noteE2 = new HashMap<>();
        noteE2.put("lftc", Arrays.asList(9.0));
        clasa.put(e1, noteE1);
        clasa.put(e2, noteE2);

        repo.setClasa(clasa);
        medii = repo.calculeazaMedii();

        Assert.assertEquals(medii.size(), 2);
        medii.sort(Comparator.comparingInt(m -> m.getElev().getNrmatricol()));
        m1 = medii.get(0);
        m2 = medii.get(1);
        Assert.assertEquals(m1.getElev().getNrmatricol(), e1.getNrmatricol());
        Assert.assertEquals(m1.getElev().getNume(), e1.getNume());
        Assert.assertEquals(m1.getMedie(), 8, 0.001);
        Assert.assertEquals(m2.getElev().getNrmatricol(), e2.getNrmatricol());
        Assert.assertEquals(m2.getElev().getNume(), e2.getNume());
        Assert.assertEquals(m2.getMedie(), 9, 0.001);
    }

    @Test
    public void test4() throws ClasaException {
        HashMap<String, List<Double>> noteE1 = new HashMap<>();
        noteE1.put("lftc", Arrays.asList(8.0, 7.0));
        noteE1.put("asc", Arrays.asList(10.0, 8.0));
        HashMap<String, List<Double>> noteE2 = new HashMap<>();
        noteE2.put("lftc", Arrays.asList(3.0, 6.0));
        noteE2.put("asc", Arrays.asList(5.0));
        clasa.put(e1, noteE1);
        clasa.put(e2, noteE2);

        repo.setClasa(clasa);
        medii = repo.calculeazaMedii();

        Assert.assertEquals(medii.size(), 2);
        medii.sort(Comparator.comparingInt(m -> m.getElev().getNrmatricol()));
        m1 = medii.get(0);
        m2 = medii.get(1);
        Assert.assertEquals(m1.getElev().getNrmatricol(), e1.getNrmatricol());
        Assert.assertEquals(m1.getElev().getNume(), e1.getNume());
        Assert.assertEquals(m1.getMedie(), 8.25, 0.001);
        Assert.assertEquals(m2.getElev().getNrmatricol(), e2.getNrmatricol());
        Assert.assertEquals(m2.getElev().getNume(), e2.getNume());
        Assert.assertEquals(m2.getMedie(), 4.75, 0.001);
    }
}
