package nrir2158MV.mytests;

import nrir2158MV.controller.NoteController;
import nrir2158MV.utils.ClasaException;
import nrir2158MV.utils.Constants;
import org.apache.commons.lang3.StringUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.junit.Assert.assertEquals;

public class F01Test {

    private NoteController ctrl;

    @Before
    public void setUp() throws Exception {
        ctrl = new NoteController();
    }

    @After
    public void tearDown() throws Exception {
    }

    @Rule
    public ExpectedException expectedEx = ExpectedException.none();

    @Test
    public void test1() throws ClasaException {
        ctrl.addNota(248, "Arhitectura sistemelor de calcul", 8.8);
        assertEquals(1, ctrl.getNote().size());
    }

    @Test
    public void test2() throws ClasaException {
        expectedEx.expect(ClasaException.class);
        expectedEx.expectMessage(Constants.invalidNota);
        ctrl.addNota(842, "Calcul numeric", 0.0);
    }

    @Test
    public void test3() throws ClasaException {
        expectedEx.expect(ClasaException.class);
        expectedEx.expectMessage(Constants.invalidNota);
        ctrl.addNota(842, "Calcul numeric", 13.4);
    }

//    @Test
//    public void test4() throws ClasaException {
//        expectedEx.expect(ClasaException.class);
//        expectedEx.expectMessage(Constants.invalidMateria);
//        ctrl.addNota(178, 3256, 9.3);
//    }

//    @Test
//    public void test5() throws ClasaException {
//        expectedEx.expect(ClasaException.class);
//        expectedEx.expectMessage(Constants.invalidMateria);
//        ctrl.addNota(154, "Analiza Matematica", "restanta");
//    }

    @Test
    public void test6() throws ClasaException {
        expectedEx.expect(ClasaException.class);
        expectedEx.expectMessage(Constants.invalidNota);
        ctrl.addNota(324, "ASC", 0.9);
    }

    @Test
    public void test7() throws ClasaException {
        ctrl.addNota(547, "VVSS", 1.0);
        assertEquals(1, ctrl.getNote().size());
    }

    @Test
    public void test8() throws ClasaException {
        ctrl.addNota(543, "CN", 1.1);
        assertEquals(1, ctrl.getNote().size());
    }

    @Test
    public void test9() throws ClasaException {
        ctrl.addNota(247, "PPD", 9.9);
        assertEquals(1, ctrl.getNote().size());
    }

    @Test
    public void test10() throws ClasaException {
        ctrl.addNota(843, "OOP", 10.0);
        assertEquals(1, ctrl.getNote().size());
    }

    @Test
    public void test11() throws ClasaException {
        expectedEx.expect(ClasaException.class);
        expectedEx.expectMessage(Constants.invalidNota);
        ctrl.addNota(876, "PLF", 11.1);
    }

//    @Test
//    public void test12() throws ClasaException {
//        expectedEx.expect(ClasaException.class);
//        expectedEx.expectMessage(Constants.invalidNota);
//        ctrl.addNota(547, "Algebra", "abc");
//    }

    @Test
    public void test13() throws ClasaException {
        expectedEx.expect(ClasaException.class);
        expectedEx.expectMessage(Constants.invalidMateria);
        ctrl.addNota(663, "", 6.0);
    }

    @Test
    public void test14() throws ClasaException {
        ctrl.addNota(182, "A", 8.0);
        assertEquals(1, ctrl.getNote().size());
    }

    @Test
    public void test15() throws ClasaException {
        String materie = StringUtils.repeat("a", 100);
        ctrl.addNota(841, materie, 8.0);
        assertEquals(1, ctrl.getNote().size());
    }

    @Test
    public void test16() throws ClasaException {
        expectedEx.expect(ClasaException.class);
        expectedEx.expectMessage(Constants.invalidMateria);
        String materie = StringUtils.repeat("a", 101);
        ctrl.addNota(258, materie, 4.0);
    }

    @Test
    public void test17() throws ClasaException {
        String materie = StringUtils.repeat("a", 99);
        ctrl.addNota(258, materie, 4.0);
        assertEquals(1, ctrl.getNote().size());
    }
}