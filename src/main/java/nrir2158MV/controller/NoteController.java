package nrir2158MV.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import nrir2158MV.model.Corigent;
import nrir2158MV.model.Elev;
import nrir2158MV.model.Medie;
import nrir2158MV.model.Nota;
import nrir2158MV.repository.ClasaRepository;
import nrir2158MV.repository.ClasaRepositoryMock;
import nrir2158MV.repository.EleviRepository;
import nrir2158MV.repository.EleviRepositoryMock;
import nrir2158MV.repository.NoteRepository;
import nrir2158MV.repository.NoteRepositoryMock;
import nrir2158MV.utils.ClasaException;

public class NoteController {
    private NoteRepository note;
    private ClasaRepository clasa;
    private EleviRepository elevi;

    public NoteController() {
        note = new NoteRepositoryMock();
        clasa = new ClasaRepositoryMock();
        elevi = new EleviRepositoryMock();
    }

    public void setClasa(ClasaRepository clasa) {
        this.clasa = clasa;
    }

    public ClasaRepository getClasaRepo() {
        return clasa;
    }

    public EleviRepository getEleviRepo() {
        return elevi;
    }

    public NoteRepository getNoteRepo() {
        return note;
    }

    public void addNota(Nota nota) throws ClasaException {
        note.addNota(nota);
    }

    public void addNota(int nr_matricol, String materie, double nota_acordata) throws ClasaException {
        Nota nota = new Nota(nr_matricol, materie, nota_acordata);
        note.addNota(nota);
    }

    public void addElev(Elev elev) {
        elevi.addElev(elev);
    }

    public void creeazaClasa(List<Elev> elevi, List<Nota> note) {
        clasa.creazaClasa(elevi, note);
    }

    public List<Medie> calculeazaMedii() throws ClasaException {
        return clasa.calculeazaMedii();
    }

    public List<Nota> getNote() {
        return note.getNote();
    }

    public List<Elev> getElevi() {
        return elevi.getElevi();
    }

    public HashMap<Elev, HashMap<String, List<Double>>> getClasa() {
        return clasa.getClasa();
    }

    public void afiseazaClasa() {
        clasa.afiseazaClasa();
    }

    public void readElevi(String fisier) {
        elevi.readElevi(fisier);
    }

    public void readNote(String fisier) {
        note.readNote(fisier);
    }

    public List<Corigent> getCorigenti() throws ClasaException {
        return clasa.getCorigenti();
    }
}
