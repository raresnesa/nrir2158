package nrir2158MV.repository;

import nrir2158MV.model.Corigent;
import nrir2158MV.model.Elev;
import nrir2158MV.model.Medie;
import nrir2158MV.model.Nota;
import nrir2158MV.utils.ClasaException;
import nrir2158MV.utils.Constants;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

public class ClasaRepositoryMock implements ClasaRepository {

    private HashMap<Elev, HashMap<String, List<Double>>> clasa;

    public ClasaRepositoryMock() {
        clasa = new HashMap<>();
    }

    @Override
    public void creazaClasa(List<Elev> elevi, List<Nota> note) {
        // TODO Auto-generated method stub
        List<String> materii = new ArrayList<>();
        for (Nota nota : note) {
            if (!materii.contains(nota.getMaterie()))
                materii.add(nota.getMaterie());
        }
        for (Elev elev : elevi) {
            HashMap<String, List<Double>> situatie = new HashMap<String, List<Double>>();
            for (String materie : materii) {
                List<Double> noteMaterie = new ArrayList<>();
                for (Nota nota : note)
                    if (nota.getMaterie().equals(materie) && nota.getNrmatricol() == elev.getNrmatricol())
                        noteMaterie.add(nota.getNota());
                situatie.put(materie, noteMaterie);
            }
            clasa.put(elev, situatie);
        }

    }

    @Override
    public HashMap<Elev, HashMap<String, List<Double>>> getClasa() {
        return clasa;
    }

    public void setClasa(HashMap<Elev, HashMap<String, List<Double>>> clasa) {
        this.clasa = clasa;
    }

    @Override
    public List<Medie> calculeazaMedii() throws ClasaException {
        List<Medie> medii = new LinkedList<>(); // 1
        if (clasa.size() > 0) { // 2
            for (Elev elev : clasa.keySet()) { // 4
                Medie medie = new Medie(); // 5
                medie.setElev(elev); // 5
                int nrMaterii = 0; // 5
                double sumaMedii = 0; // 5
                double medieElev = 0; // 5
                for (String materie : clasa.get(elev).keySet()) { // 6
                    nrMaterii++; // 8
                    List<Double> noteElev = clasa.get(elev).get(materie); // 8
                    int nrNote = noteElev.size(); // 8
                    int i = 0; // 8
                    double suma = 0; // 8
                    if (nrNote > 0) { // 9
                        while (i < nrNote) { // 10
                            double nota = noteElev.get(i); // 11
                            suma += nota; // 11
                            i++; // 11
                        }
                        sumaMedii = sumaMedii + suma / i; // 12
                    }
                }
                medieElev = sumaMedii / nrMaterii; // 7
                medie.setMedie(medieElev); // 7
                medii.add(medie); // 7
            }
        } else {
            throw new ClasaException(Constants.emptyRepository); // 3
        }
        return medii; // 13
    }

    public void afiseazaClasa() {
        for (Elev elev : clasa.keySet()) {
            System.out.println(elev);
            for (String materie : clasa.get(elev).keySet()) {
                System.out.println(materie);
                for (double nota : clasa.get(elev).get(materie))
                    System.out.print(nota + " ");
            }
        }
    }

    @Override
    public List<Corigent> getCorigenti() throws ClasaException {
        List<Corigent> listaCorigenti = new ArrayList<Corigent>();
        if (clasa.size() > 0) {
            for (Elev elev : clasa.keySet()) {
                Corigent corigent = new Corigent(elev.getNume(), 0);
                for (String materie : clasa.get(elev).keySet()) {
                    List<Double> noteElev = clasa.get(elev).get(materie);
                    int nrNote = noteElev.size();
                    int i = 0;
                    double suma = 0;
                    if (nrNote > 0) {
                        while (i < nrNote) {
                            double nota = noteElev.get(i);
                            suma += nota;
                            i++;
                        }
                        double media = suma / i;
                        if (media < 4.5)
                            corigent.setNrMaterii(corigent.getNrMaterii() + 1);
                    }
                }
                if (corigent.getNrMaterii() > 0) {
                    int i = 0;
                    while (i < listaCorigenti.size() && listaCorigenti.get(i).getNrMaterii() < corigent.getNrMaterii())
                        i++;
                    if (i != listaCorigenti.size() && listaCorigenti.get(i).getNrMaterii() == corigent.getNrMaterii()) {
                        while (i < listaCorigenti.size() && listaCorigenti.get(i).getNrMaterii() == corigent.getNrMaterii() && listaCorigenti.get(i).getNumeElev().compareTo(corigent.getNumeElev()) < 0)
                            i++;
                        listaCorigenti.add(i, corigent);
                    } else
                        listaCorigenti.add(i, corigent);
                }
            }
        } else {
            throw new ClasaException(Constants.emptyRepository);
        }
        return listaCorigenti;
    }

}
