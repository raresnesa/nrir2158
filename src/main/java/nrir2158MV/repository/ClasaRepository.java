package nrir2158MV.repository;

import nrir2158MV.model.Corigent;
import nrir2158MV.model.Elev;
import nrir2158MV.model.Medie;
import nrir2158MV.model.Nota;
import nrir2158MV.utils.ClasaException;

import java.util.HashMap;
import java.util.List;

public interface ClasaRepository {
	
	public void creazaClasa(List<Elev> elevi, List<Nota> note);
	public HashMap<Elev, HashMap<String, List<Double>>> getClasa();
	public List<Medie> calculeazaMedii() throws ClasaException;
	public void afiseazaClasa();
	public List<Corigent> getCorigenti() throws ClasaException;
}
