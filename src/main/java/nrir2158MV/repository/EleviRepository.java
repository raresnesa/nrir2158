package nrir2158MV.repository;

import java.util.List;

import nrir2158MV.model.Elev;

public interface EleviRepository {
	public void addElev(Elev e);
	public List<Elev> getElevi();
	public void readElevi(String fisier);
}
