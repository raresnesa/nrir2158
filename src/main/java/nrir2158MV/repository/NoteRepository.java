package nrir2158MV.repository;

import java.util.List;

import nrir2158MV.utils.ClasaException;

import nrir2158MV.model.Nota;

public interface NoteRepository {
	
	public void addNota(Nota nota) throws ClasaException;
	public List<Nota> getNote(); 
	public void readNote(String fisier);
	
}
